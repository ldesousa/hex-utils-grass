#!/usr/bin/env python
# -*- coding: utf-8 -*-

############################################################################
#
# MODULE:       h.in
#
# AUTHOR(S):    Luís Moreira de Sousa
#
# PURPOSE:      Exports an hexagonal raster from GRASS to an HexASCII file
#
# COPYRIGHT:    (c) 2018 Luís Moreira de Sousa
#
#               This programme is released under the European Union Public
#               Licence v 1.1. Please consult the LICENCE file for details.
#
#############################################################################

#%module
#% description: Exports an hexagonal raster from GRASS to an HexASCII file.
#% keyword: HexASCII
#% keyword: hexagons
#% keyword: raster
#%end
#%option INPUT
#% key: input
#% description: HexASCII raster in the GRASS database.
#%end
#%option OUTPUT
#% key: output
#% description: Path of the HexASCII raster to create on disk.
#%end

import grass.script as gscript
from grass.pygrass import raster
from grass.pygrass import utils
from grass.pygrass.raster.buffer import Buffer
from hex_utils.hasc import HASC

def main():

    options, flags = gscript.parser()
    input = options['input']
    output = options['output']

    if(input is None or input == ""):
        gscript.error(_("[h.out]: input is a mandatory parameter."))
        exit()

    exists = False
    maps_list = utils.findmaps(type='raster')
    for map in maps_list:
        if input == map[0]:
            exists = True
            break
    if(not exists):
        gscript.error(_("[h.out]: could not find input map."))
        exit()

    if(output is None or output == ""):
        gscript.error(_("[h.out]: output is a mandatory parameter."))
        exit()

    rast = raster.RasterRow(input)
    rast.open()

    hexASCII = HASC()
    hexASCII.init(int(rast.info.cols),
                  int(rast.info.rows),
                float(rast.info.east),
                float(rast.info.south),
                float(rast.info.ewres),
                "NA")

    for row in range(0, hexASCII.nrows):
        for col in range(0, hexASCII.ncols):
            hexASCII.set(col, row, rast[row][col])
        gscript.message(_("[h.in] DEBUG: Exporting row: %s" % row))

    try:
        hexASCII.save(output)

    except (ValueError, IOError) as ex:
        gscript.error(_("[h.out] ERROR: Failed to save HexASCII file %s: %s" % (output, ex)))
        exit()

    gscript.message(_("[h.out] SUCCESS: HexASCII raster exported."))

if __name__ == '__main__':
    main()
